

 // js product infomation
 $(document).ready(function () {
  $("#content-slider").lightSlider({
    loop: true,
    keyPress: true
  });
  $('#image-gallery').lightSlider({
    gallery: true,
    item: 1,
    thumbItem: 9,
    slideMargin: 0,
    // speed:500,
    // auto:true,
    // loop:true,
    // autoplay:false,
    onSliderLoad: function () {
      $('#image-gallery').removeClass('cS-hidden');
    }
  });
});


/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
// End Js  select box

$(".faqs-item").click(function () {
  $('.faqs-item').removeClass('active');
  $(this).addClass('active');
  });


 

$(document).ready(function () {
  $('.about-home-slide').owlCarousel({
    loop: true,
    rewind: true,
    items: 1,
    margin: 10,
    // autoplayHoverPause: false,
    // autoplay: 700,
    // smartSpeed: 700,
    responsiveClass: true,
    items: 1,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      900: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

$(document).ready(function () {
  $('.about-home-slide-2').owlCarousel({
    loop: true,
    rewind: true,
    items: 2,
    margin: 10,
    // autoplayHoverPause: false,
    // autoplay: 700,
    // smartSpeed: 700,
    responsiveClass: true,
    items: 1,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 2,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 2,
        nav: true,
        loop: false,
      }
    }
  })
});


$(".head-filter").click(function () {
  $('.sub-mobile-filter').removeClass('active');
});
$(".title-filter-mb button").click(function () {
  $('.sub-mobile-filter').addClass('active');
});

$("#supplier-btn").click(function () {
  $('.form-contact-page').removeClass('active');
  $('#supplier').addClass('active');
});
$("#buyer-btn").click(function () {
  $('.form-contact-page').removeClass('active');
  $('#buyer').addClass('active');
});

$(".action-contact-form button").click(function () {
  $('.action-contact-form button').removeClass('active');
  $(this).addClass('active');
});

// Js fillter search-result
$(".filter-mb-results").click(function () {
  $('.left-sidebar').addClass('active');
});
$(".close-filter").click(function () {
  $('.left-sidebar').removeClass('active');
});

$(".content-sub-filter ul li").click(function () {
  $(this).toggleClass('active');
});

$(".item-menu-footer").click(function () {
  $('.item-menu-footer').removeClass('active');
  $(this).addClass('active');
});

$(".show-mb-btn").click(function () {
    $('#navbar').addClass('active');
});
$(".close-mb-btn").click(function () {
    $('#navbar').removeClass('active');
});

$(".language-mobile").click(function () {
    $('.menu-languages-show').addClass('active');
});
$(".menu-languages-show__head img").click(function () {
    $('.menu-languages-show').removeClass('active');
});
// Js Menu mobile header 1
$(".img-show-search").click(function () {
  $('.sub-search').addClass('active');
});
$(".close-sub-search").click(function () {
  $('.sub-search').removeClass('active');
});



$(".content-sub-filter ul li").click(function () {
  $(this).toggleClass('active');
});

$(".item-menu-footer").click(function () {
  $('.item-menu-footer').removeClass('active');
  $(this).addClass('active');
});

$(function() {
  // Owl Carousel
  var owl = $(".owl-carousel");
  owl.owlCarousel({
    items: 2,
    margin: 10,
    loop: true,
    nav: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      }
    }
  });
});



$(document).on('change', ".fileUploadWrap input[type='file']",function(){
  if ($(this).val()) {

      var filename = $(this).val().split("\\");
   
      filename = filename[filename.length-1];

      $('.fileName').text(filename);
  }
});
$('.center').slick({
    centerMode: true,
    centerPadding: '180px', // defines other partial showing slides width
    slidesToShow: 1,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });


  $('.center-mb').slick({
    centerMode: true,
    centerPadding: '40px', // defines other partial showing slides width
    slidesToShow: 1,
  });

  
//scroll up down menu
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-155px";
  }
  prevScrollpos = currentScrollPos;
}

const swiper = new Swiper('.sample-slider', {
    loop: true,
    speed: 2000,
    slidesPerView: 4,      
    autoplay: {
        delay: 0,
    },
    responsive: {
        600: {
            slidesPerView: 2  
        },
      }
})

// Show the first tab and hide the rest
$('#tabs-nav li:first-child').addClass('active');
$('.tab-content').hide();
$('.tab-content:first').show();

// Click function
$('#tabs-nav li').click(function(){
  $('#tabs-nav li').removeClass('active');
  $(this).addClass('active');
  $('.tab-content').hide();
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});







$(document).ready(function () {
  window.onscroll = function() {myFunction()};
  var header = document.getElementById("navbar");
  var sticky = header.offsetTop;
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  }
});


// <=============arena-slide============>
$(document).ready(function () {
  $('.arena-slide').owlCarousel({
    loop: true,
    rewind: true,
    items: 1,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: true,
    smartSpeed: 700,
    autoplayTimeout:5000,
    responsiveClass: true,
    items: 1,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      900: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// <=================Popup=====================>
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
})


// <=============racekit-slide============>
$(document).ready(function () {
  $('.racekit-slide').owlCarousel({
    loop: true,
    rewind: true,
    items: 1,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: true,
    smartSpeed: 700,
    autoplayTimeout:5000,
    responsiveClass: true,
    items: 1,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      900: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});


// <==============botton-lis===================>
$('.item-at').on('click', function(){
  $('.title-at').removeClass('active');
  $(this).toggleClass('active');
});
// <=====================COUNTDOWN====================>
function timeCountDown () {
  const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

  //I'm adding this section so I don't have to keep updating this pen every year :-)
  //remove this if you don't need it
  let today = new Date(),
      dd = String(today.getDate()).padStart(2, "0"),
      mm = String(today.getMonth() + 1).padStart(2, "0"),
      yyyy = today.getFullYear(),
      nextYear = yyyy + 1,
      dayMonth = "09/30/",
      birthday = dayMonth + yyyy;
  
  today = mm + "/" + dd + "/" + yyyy;
  if (today > birthday) {
    birthday = dayMonth + nextYear;
  }
  //end
  
  const countDown = new Date(birthday).getTime(),
      x = setInterval(function() {    

        const now = new Date().getTime(),
              distance = countDown - now;

        document.getElementById("days").innerText = Math.floor(distance / (day)),
          document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
          document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
          document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

        //do something later when date is reached
        if (distance < 0) {
          document.getElementById("headline").innerText = "It's my birthday!";
          document.getElementById("countdown").style.display = "none";
          document.getElementById("content").style.display = "block";
          clearInterval(x);
        }
        //seconds
      }, 0)
  };



$(document).ready(function () {
  $('.about-home-slide').owlCarousel({
    loop: true,
    rewind: true,
    items: 1,
    margin: 10,
    // autoplayHoverPause: false,
    // autoplay: 700,
    // smartSpeed: 700,
    responsiveClass: true,
    items: 1,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      900: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

$(".head-filter").click(function () {
  $('.sub-mobile-filter').removeClass('active');
});
$(".title-filter-mb button").click(function () {
  $('.sub-mobile-filter').addClass('active');
});

$("#supplier-btn").click(function () {
  $('.form-contact-page').removeClass('active');
  $('#supplier').addClass('active');
});
$("#buyer-btn").click(function () {
  $('.form-contact-page').removeClass('active');
  $('#buyer').addClass('active');
});

$(".action-contact-form button").click(function () {
  $('.action-contact-form button').removeClass('active');
  $(this).addClass('active');
});


function onMenuToggle(e) {
  const navlinks = document.querySelector(".navLinks");
  e.name = e.name === "menu" ? "close" : "menu";
  navlinks.classList.toggle("left-[0%]");
}


$(".btn-open-menu").click(function () {
    $('header').addClass('active');
});
$(".btn-close-menu").click(function () {
    $('header').removeClass('active');
});

// Js Menu mobile header 1
$(".btn-open-menu").click(function () {
  $('.sub-search').addClass('active');
});
$(".close-sub-search").click(function () {
  $('.sub-search').removeClass('active');
});



$(".content-sub-filter ul li").click(function () {
  $(this).toggleClass('active');
});

$(".item-menu-footer").click(function () {
  $('.item-menu-footer').removeClass('active');
  $(this).addClass('active');
});

$(function() {
  // Owl Carousel
  var owl = $(".owl-carousel");
  owl.owlCarousel({
    items: 2,
    margin: 10,
    loop: true,
    nav: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      }
    }
  });
});



$(document).on('change', ".fileUploadWrap input[type='file']",function(){
  if ($(this).val()) {

      var filename = $(this).val().split("\\");
   
      filename = filename[filename.length-1];

      $('.fileName').text(filename);
  }
});
$(document).ready(function () {
// $('.center').slick({
//     centerMode: true,
//     centerPadding: '180px', // defines other partial showing slides width
//     slidesToShow: 1,
//     responsive: [
//       {
//         breakpoint: 480,
//         settings: {
//           arrows: false,
//           centerMode: true,
//           centerPadding: '40px',
//           slidesToShow: 1
//         }
//       }
//     ]
//   });


//   $('.center-mb').slick({
//     centerMode: true,
//     centerPadding: '40px', // defines other partial showing slides width
//     slidesToShow: 1,
//   });
});
  
//scroll up down menu
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-155px";
  }
  prevScrollpos = currentScrollPos;
}

// const swiper = new Swiper('.sample-slider', {
//     loop: true,
//     speed: 2000,
//     slidesPerView: 4,      
//     autoplay: {
//         delay: 0,
//     },
//     responsive: {
//         600: {
//             slidesPerView: 2  
//         },
//       }
// })



// Show the first tab and hide the rest
$('#tabs-nav li:first-child').addClass('active');
$('.tab-content').hide();
$('.tab-content:first').show();

// Click function
$('#tabs-nav li').click(function(){
  $('#tabs-nav li').removeClass('active');
  $(this).addClass('active');
  $('.tab-content').hide();
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});


// <!-- <==============Pagination===============> -->
document.addEventListener("DOMContentLoaded", function () {
  const pages = document.querySelectorAll(".page");
  const pageNumbers = document.querySelectorAll(".page-number");
  const prevButton = document.getElementById("prevPage");
  const nextButton = document.getElementById("nextPage");
  let currentPage = 0;

  function showPage(pageNumber) {
      pages.forEach((page, index) => {
          if (index === pageNumber) {
              page.style.display = "block";
          } else {
              page.style.display = "none";
          }
      });
  }

  function updateButtons() {
      prevButton.disabled = currentPage === 0;
      nextButton.disabled = currentPage === pages.length - 1;
  }

  function setActive() {
      pageNumbers.forEach((page, index) => {
          if(currentPage === index) {
              page.classList.add("active");
          } else {
              page.classList.remove("active");
          }
      });
  }

  pageNumbers.forEach((page, index) => {
      page.addEventListener("click", function () {
          showPage(index);
          currentPage = index;
          updateButtons();
          setActive();
      });
  });

  prevButton.addEventListener("click", function () {
      if (currentPage > 0) {
          currentPage--;
          showPage(currentPage);
          updateButtons();
          setActive();
      }
  });

  nextButton.addEventListener("click", function () {
      if (currentPage < pages.length - 1) {
          currentPage++;
          showPage(currentPage);
          updateButtons();
          setActive();
      }
  });

  showPage(currentPage);
  updateButtons();
});

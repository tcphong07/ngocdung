

// Js Menu mobile header
$(".menu-mobile .ti-menu").click(function () {
  $('.header').addClass('show-menu');
});
$(".close-menu").click(function () {
  $('.header').removeClass('show-menu');
});
//Search mobile 
$(".btn-search-mb .ti-search-mb").click(function () {
  $('.header').addClass('show-search-mb');
});
$(".close-search-10").click(function () {
  $('.header').removeClass('show-search-mb');

});
$(window).scroll(function () {
  let contentHeadHeigh = $(".menu-tab-blogs").height();
  var sticky = $('.menu-tab-blogs'),
    scroll = $(window).scrollTop();

  if (scroll >= contentHeadHeigh) sticky.addClass('menu-scroll');
  if (scroll == 0)
    sticky.removeClass('menu-scroll');
});
// End Js Menu mobile header 

// CAU HOI THUONG GAP //
/*******************************
* ACCORDION WITH TOGGLE ICONS
*******************************/
function toggleIcon(e) {
  $(e.target)
    .prev('.panel-heading')
    .find(".more-less")
    .toggleClass('ti-angle-down ti-angle-up');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

// js product infomation
$(document).ready(function () {
  $("#content-slider").lightSlider({
    loop: true,
    keyPress: true
  });
  $('#image-gallery').lightSlider({
    gallery: true,
    item: 1,
    thumbItem: 9,
    slideMargin: 0,
    // speed:500,
    // auto:true,
    // loop:true,
    // autoplay:false,
    onSliderLoad: function () {
      $('#image-gallery').removeClass('cS-hidden');
    }
  });
});

// Input- file with file name
jQuery("#inputFile").on('change', function () {

  var file = this.files[0].name;
  var fileTitle = jQuery(this).attr("placeholder");
  if (jQuery(this).val() !== "") {
    jQuery(this).next().text(file);
  } else {
    jQuery(this).next().text(fileTitle);
  }
});

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
// End Js  select box

// Elements Slider  homepage
$(document).ready(function () {
  $('.slider-home').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});

// Elements Slider  list-partners about
$(document).ready(function () {
  $('.list-partners').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 2,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements Slider product homepage
$(document).ready(function () {
  $('.slider-product-home').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 4,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 4,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements Slider product homepage
$(document).ready(function () {
  $('.slider-project-home').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// Elements Slider  homepage
$(document).ready(function () {
  $('.slider-project-related').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
// slider customer-feedback page career
$(document).ready(function () {
  $('.slider-customer-feedback').owlCarousel({
    loop: true,
    rewind: true,
    margin: 10,
    autoplayHoverPause: false,
    autoplay: 700,
    smartSpeed: 700,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 1,
        nav: false
      },
      1000: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      },
      1300: {
        items: 1,
        nav: true,
        loop: false,
        margin: 0
      }
    }
  })
});
//Js timeline page about
var timelineElement = document.getElementById('timeline-aboutus');

if (window.Timeline && timelineElement) {
  new Timeline({
    element: timelineElement,
  });
}
// Js section who are you homepage
const overlayTrigger = document.querySelector('.overlay-launch'),
  overlay = document.querySelector('.overlay'),
  tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";

const firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
let player,
  source;



function onYouTubeIframeAPIReady(evt) {
  console.log(source);
  player = new YT.Player('player', {
    height: '600px',
    width: '640'
  });
}


function launchOverlay(evt) {
  const $trigger = evt.target;
  source = $trigger.dataset.src;
  overlay.classList.toggle('_active');
  player.loadVideoById(source, 5, "large")


  if (!overlay.classList.contains('_active')) {
    player.stopVideo();
  } else {
    player.playVideo();
  }
}

overlayTrigger.addEventListener('click', launchOverlay);


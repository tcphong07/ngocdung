
// <========================POPUP SHOW HIDEN==========================>
// Elements Slider  homepage
$(document).ready(function () {
    $('.slider-home').owlCarousel({
      loop: true,
      rewind: true,
      margin: 10,
      autoplayHoverPause: false,
      autoplay: 700,
      smartSpeed: 700,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        },
        1300: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        }
      }
    })
  });


  // Elements Slider  Career
$(document).ready(function () {
    $('.slider-career').owlCarousel({
      loop: true,
      rewind: true,
      margin: 10,
      autoplayHoverPause: false,
      autoplay: 700,
      smartSpeed: 700,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        },
        1300: {
          items: 1,
          nav: true,
          loop: false,
          margin: 0
        }
      }
    })
  });

  // tab  Career
  function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;
  
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
  
    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
  
    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }
 // end tab  Career

//tab  bar min
let tabsBox = document.querySelector('.tabs_box'),
arrowIcons = document.querySelectorAll('.icon i'),
allTabs = document.querySelectorAll('.tab')

const handleIcons = ()=>{
    let scrollVal = Math.round(tabsBox.scrollLeft)
    let maxScrollableWidth = tabsBox.scrollWidth - tabsBox.clientWidth;
    arrowIcons[0].parentElement.style.display = scrollVal > 0? "flex" : "none";
    arrowIcons[1].parentElement.style.display = maxScrollableWidth > scrollVal? "flex" : "none";
}

arrowIcons.forEach(icon => {
    icon.addEventListener("click", ()=>{
        // if clicked icon is left, reduce 350 from tabsBox scrollLeft is add
        tabsBox.scrollLeft += icon.id === "left" ? -350 : 350;
        setTimeout(()=>handleIcons(),50)

    })
});

allTabs.forEach(tab => {
    tab.addEventListener("click", ()=>{
        tabsBox.querySelector('.active').classList.remove('active')
        tab.classList.add('active')

    })
});

let isDragging = false;

const dragging = (e)=>{
    if(!isDragging) return;
    tabsBox.classList.add('dragging')
    tabsBox.scrollLeft -= e.movementX;
    handleIcons()
}

const dragstop = ()=>{
    isDragging = false;
    tabsBox.classList.remove('dragging')
}

tabsBox.addEventListener('mousedown', ()=>isDragging = true)
tabsBox.addEventListener('mousemove', dragging)
document.addEventListener('mouseup', dragstop)  

// <====================POPUP-AD-GAME==========================>
  $('#myModal').modal(options)



// <====================SEARCH==========================>
const resultsUI = document.querySelector('.results');
const searchInput = document.querySelector('.search');
const empty = document.querySelector('.empty');
let movies;

init();
searchInput.addEventListener('keyup', handleSearch);

async function init () {
    movies = (await getMovies()).results;
    renderUI(movies);
}

function handleSearch (e) {
    if (!movies.length) {
        return;
    }

    const inputValue = e.target.value.toLowerCase();

    const filteredMovies = movies.filter((movie) => {
        return (movie.title.toLowerCase().indexOf(inputValue) > -1 ||
               movie.opening_crawl.indexOf(inputValue) > -1)
    });
                                    
    renderUI(filteredMovies);
}

function renderUI (movies) {
    console.log(movies.length);
    if (!movies.length) {
        empty.classList.add('active');
        resultsUI.innerHTML = '';
        return;
    }

    empty.classList.remove('active');
    
    let list = '';

    for (let i = 0; i < movies.length; i++) {
        const movie = movies[i];

        list += `
            <li class="result">
                <article class="card">
                    <h1 class="card__title">${movie.title}</h1>
                    <p class="card__crawl">${movie.opening_crawl}</p>
                </article>
            </li>
        `;
    }
    
    resultsUI.innerHTML = list;
}

async function getMovies () {
    return fetch('https://swapi.dev/api/films')
        .then(resp => resp.json());
}

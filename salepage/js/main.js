// <================Countdown===================>
  function updateCountdown() {
    const targetDate = new Date("2025-01-01T00:00:00");
    const now = new Date();
    const timeDifference = targetDate - now;
  
    if (timeDifference <= 0) {
      document.getElementById("months").innerText = "00";
      document.getElementById("days").innerText = "00";
      document.getElementById("hours").innerText = "00";
      document.getElementById("minutes").innerText = "00";
      document.getElementById("seconds").innerText = "00";
      return;
    }
  
    const months = Math.floor(timeDifference / (1000 * 60 * 60 * 24 * 30));
    const days = Math.floor(
      (timeDifference % (1000 * 60 * 60 * 24 * 30)) / (1000 * 60 * 60 * 24)
    );
    const hours = Math.floor(
      (timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000);
  
    document.getElementById("months").innerText = String(months).padStart(2, "0");
    document.getElementById("days").innerText = String(days).padStart(2, "0");
    document.getElementById("hours").innerText = String(hours).padStart(2, "0");
    document.getElementById("minutes").innerText = String(minutes).padStart(
      2,
      "0"
    );
    document.getElementById("seconds").innerText = String(seconds).padStart(
      2,
      "0"
    );
  }
  
  setInterval(updateCountdown, 1000);
// <================End Countdown===================>

// <================Video===================>
    $(".ndvideo").on('click', function(e) {
        e.preventDefault();
        $("#video-popup-overlay,#video-popup-iframe-container,#video-popup-container,#video-popup-close").show();
        
        var srchref='',autoplay='',id=$(this).data('id');
        if($(this).data('type') == 'vimeo') var srchref="//player.vimeo.com/video/";
        else if($(this).data('type') == 'youtube') var srchref="https://www.youtube.com/embed/";
        
        if($(this).data('autoplay') == true) autoplay = '?autoplay=1';
        
        $("#video-popup-iframe").attr('src', srchref+id+autoplay);
        
        $("#video-popup-iframe").on('load', function() {
          $("#video-popup-container").show();
        });
      });
      
      $("#video-popup-close, #video-popup-overlay").on('click', function(e) {
        $("#video-popup-iframe-container,#video-popup-container,#video-popup-close,#video-popup-overlay").hide();
        $("#video-popup-iframe").attr('src', '');
      });
      // <================End Video===================>


// <================Video 2 gioi thieu ND ==================>
  $(".ndvideo2").on('click', function(e) {
    e.preventDefault();
    $("#video-popup-overlay2,#video-popup-iframe-container2,#video-popup-container2,#video-popup-close2").show();
    
    var srchref='',autoplay='',id=$(this).data('id');
    if($(this).data('type') == 'vimeo') var srchref="//player.vimeo.com/video/";
    else if($(this).data('type') == 'youtube') var srchref="https://www.youtube.com/embed/";
    
    if($(this).data('autoplay') == true) autoplay = '?autoplay=1';
    
    $("#video-popup-iframe2").attr('src', srchref+id+autoplay);
    
    $("#video-popup-iframe2").on('load', function() {
      $("#video-popup-container2").show();
    });
  });
  
  $("#video-popup-close2, #video-popup-overlay2").on('click', function(e) {
    $("#video-popup-iframe-container2,#video-popup-container2,#video-popup-close2,#video-popup-overlay2").hide();
    $("#video-popup-iframe2").attr('src', '');
  });
  // <================End Video===================>
  
    // <================ Video 3 trietlong ==================>
  $(".ndvideo3").on('click', function(e) {
    e.preventDefault();
    $("#video-popup-overlay3,#video-popup-iframe-container3,#video-popup-container3,#video-popup-close3").show();
    
    var srchref='',autoplay='',id=$(this).data('id');
    if($(this).data('type') == 'vimeo') var srchref="//player.vimeo.com/video/";
    else if($(this).data('type') == 'youtube') var srchref="https://www.youtube.com/embed/";
    
    if($(this).data('autoplay') == true) autoplay = '?autoplay=1';
    
    $("#video-popup-iframe3").attr('src', srchref+id+autoplay);
    
    $("#video-popup-iframe3").on('load', function() {
      $("#video-popup-container3").show();
    });
  });
  
  $("#video-popup-close3, #video-popup-overlay3").on('click', function(e) {
    $("#video-popup-iframe-container3,#video-popup-container3,#video-popup-close3,#video-popup-overlay3").hide();
    $("#video-popup-iframe2").attr('src', '');
  });
  // <================End Video===================>


/* ----------Js select box -----------------*/

/* JS Select box*/
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            $('.select-selected').addClass('active');
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
      
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* ----------ENd Js select box -----------------*/
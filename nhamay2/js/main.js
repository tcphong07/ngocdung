
$(".image-slider1").owlCarousel({
  margin:20,
  loop:true,
  autoplay:true,
  nav:true,
  navText:["←","→"],
  responsive:{
        0:{
            items:1
        },
        460:{
          items:2,
          merge:true,
                              
      },
        767:{
            items:3,
            merge:true,
                                
        },
        1000:{
            items:4
        }
  }
});



$(function() {
  // Owl Carousel
  var owl = $(".owl-carousel");
  owl.owlCarousel({
    items: 2,
    margin: 10,
    loop: true,
    nav: true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        }
    }
  });
});

//click table
$(".list-job-ulli ul li#table-click01").click(function(){
  $(this).addClass('show');
});

$(".close-link").click(function(){
  $('.list-job-ulli ul li#table-click01').removeClass('show');
});


$(".head-filter").click(function () {
  $('.sub-mobile-filter').removeClass('active');
});
$(".title-filter-mb button").click(function () {
  $('.sub-mobile-filter').addClass('active');
});

$("#supplier-btn").click(function () {
  $('.form-contact-page').removeClass('active');
  $('#supplier').addClass('active');
});
$("#buyer-btn").click(function () {
  $('.form-contact-page').removeClass('active');
  $('#buyer').addClass('active');
});

$(".action-contact-form button").click(function () {
  $('.action-contact-form button').removeClass('active');
  $(this).addClass('active');
});



$(".show-mb-btn").click(function () {
    $('.menu-mobile').addClass('active');
});
$(".close-mb-btn").click(function () {
    $('.menu-mobile').removeClass('active');
});

$(".language-mobile").click(function () {
    $('.menu-languages-show').addClass('active');
});
$(".menu-languages-show__head img").click(function () {
    $('.menu-languages-show').removeClass('active');
});
// Js Menu mobile header 1
$(".img-show-search").click(function () {
  $('.sub-search').addClass('active');
});
$(".close-sub-search").click(function () {
  $('.sub-search').removeClass('active');
});

// Js fillter search-result
$(".filter-mb-results").click(function () {
  $('.left-sidebar').addClass('active');
});
$(".close-filter").click(function () {
  $('.left-sidebar').removeClass('active');
});

$(".content-sub-filter ul li").click(function () {
  $(this).toggleClass('active');
});

$(".item-menu-footer").click(function () {
  $('.item-menu-footer').removeClass('active');
  $(this).addClass('active');
});





$(document).on('change', ".fileUploadWrap input[type='file']",function(){
  if ($(this).val()) {

      var filename = $(this).val().split("\\");
   
      filename = filename[filename.length-1];

      $('.fileName').text(filename);
  }
});
$('.center').slick({
    centerMode: true,
    centerPadding: '180px', // defines other partial showing slides width
    slidesToShow: 1,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });


  $('.center-mb').slick({
    centerMode: true,
    centerPadding: '40px', // defines other partial showing slides width
    slidesToShow: 1,
  });

  
//scroll up down menu
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-155px";
  }
  prevScrollpos = currentScrollPos;
}

const swiper = new Swiper('.sample-slider', {
    loop: true,
    speed: 2000,
    slidesPerView: 4,      
    autoplay: {
        delay: 0,
    },
    responsive: {
        600: {
            slidesPerView: 2  
        },
      }
})



// Show the first tab and hide the rest
$('#tabs-nav li:first-child').addClass('active');
$('.tab-content').hide();
$('.tab-content:first').show();

// Click function
$('#tabs-nav li').click(function(){
  $('#tabs-nav li').removeClass('active');
  $(this).addClass('active');
  $('.tab-content').hide();
  
  var activeTab = $(this).find('a').attr('href');
  $(activeTab).fadeIn();
  return false;
});


